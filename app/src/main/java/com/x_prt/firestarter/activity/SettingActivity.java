package com.x_prt.firestarter.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.appwidget.AppWidgetManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.x_prt.firestarter.ForegroundService;
import com.x_prt.firestarter.R;
import com.x_prt.firestarter.provider.MyWidget;
import com.x_prt.firestarter.receiver.BluetoothBroadcastReceiverKt;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by x_prt_admin on 15.05.19
 */
public class SettingActivity extends Activity {

    public final static String WIDGET_PREF = "widget_pref";
    public final static String WIDGET_TIME_FORMAT = "widget_time_format_";
    public final static String WIDGET_COUNT = "widget_count_";

    private int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
    private Intent resultValue;
    private SharedPreferences sp;
    private EditText etFormat;

    private String[] arrayAddress;
    private BroadcastReceiver bluetoothBroadcastReceiver;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // извлекаем ID конфигурируемого виджета
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        setContentView(R.layout.activity_setting);

        if (extras != null) {
            widgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
            // и проверяем его корректность
            if (widgetID == AppWidgetManager.INVALID_APPWIDGET_ID) {
                finish();
            }

            // формируем intent ответа
            resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);

            // отрицательный ответ
            setResult(RESULT_CANCELED, resultValue);


            sp = getSharedPreferences(WIDGET_PREF, MODE_PRIVATE);
            etFormat = (EditText) findViewById(R.id.etFormat);
            etFormat.setText(sp.getString(WIDGET_TIME_FORMAT + widgetID, "HH:mm:ss"));

            int cnt = sp.getInt(SettingActivity.WIDGET_COUNT + widgetID, -1);
            if (cnt == -1) sp.edit().putInt(WIDGET_COUNT + widgetID, 0);
        }

        bluetoothBroadcastReceiver = BluetoothBroadcastReceiverKt.getBluetoothBroadcastReceiver();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        registerReceiver(bluetoothBroadcastReceiver, intentFilter);
    }

    public void onClick(View v) {
        sp.edit().putString(WIDGET_TIME_FORMAT + widgetID, etFormat.getText().toString()).commit();
        MyWidget.updateWidget(this, AppWidgetManager.getInstance(this), widgetID);
        setResult(RESULT_OK, resultValue);
        finish();
    }

    public void onClickDialog(View v) {
        startService();
//        showDialog();
    }

    private void showDialog() {
        AlertDialog dialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose device.");

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        ArrayList<String> address = new ArrayList<>();
        ArrayList<String> name = new ArrayList<>();

        for (BluetoothDevice bt : pairedDevices) {
            address.add(bt.getAddress());
            name.add(bt.getName());
        }

        arrayAddress = new String[address.size()];
        String[] arrayName = new String[name.size()];

        address.toArray(arrayAddress);
        name.toArray(arrayName);


        builder.setSingleChoiceItems(arrayName, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Toast.makeText(SettingActivity.this, arrayAddress[i], Toast.LENGTH_SHORT).show();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    private void startService() {
        Intent myService = new Intent(this, ForegroundService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(myService);
        } else {
            startService(myService);
        }
    }
}
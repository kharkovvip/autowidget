package com.x_prt.firestarter.receiver

import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast

val BluetoothBroadcastReceiver = object : BroadcastReceiver() {
    override fun onReceive(contxt: Context?, intent: Intent?) {
        val action = intent?.getAction()
        if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
            Toast.makeText(contxt, "Connected!!!!! ", Toast.LENGTH_LONG).show()

            val startIntent = contxt?.getPackageManager()?.getLaunchIntentForPackage(contxt?.getPackageName())

            startIntent?.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            contxt?.startActivity(startIntent)
        }
        if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
            Toast.makeText(contxt, "Disconected!!!!! ", Toast.LENGTH_LONG).show()
        }
    }
}
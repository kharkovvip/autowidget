package com.x_prt.firestarter

import android.app.Service
import android.content.Intent
import android.os.IBinder
import androidx.core.app.NotificationCompat
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.Handler
import androidx.annotation.RequiresApi
import com.x_prt.firestarter.receiver.BluetoothBroadcastReceiver


class ForegroundService : Service() {
    private lateinit var mHandler: Handler
    private lateinit var mRunnable: Runnable

    override fun onBind(intent: Intent): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val id = createNotificationChannel("111","Test")
            val builder = Notification.Builder(this, id)
                    .setContentTitle("TEST")
                    .setContentText("TEST")
                    .setAutoCancel(true)

            val notification = builder.build()
            startForeground(id.toInt(), notification)

        } else {

            val builder = NotificationCompat.Builder(this)
                    .setContentTitle("TEST")
                    .setContentText("TEST")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setAutoCancel(true)

            val notification = builder.build()
            startForeground(1, notification)
        }

        val receiver = IntentFilter()
        receiver.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        registerReceiver(BluetoothBroadcastReceiver, receiver)
    }

    override fun onDestroy() {
        super.onDestroy()

    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String{
        val chan = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

}